package test;

import com.AA.util.Xls_Reader;

public class TestRunMode {

	
	public static void main(String[] args) {
		Xls_Reader x = new Xls_Reader(System.getProperty("user.dir")+"\\src\\com\\AA\\xls\\AlleginatAirWeb.xlsx");
		System.out.println(isTestCaseRunnable(x,"BAT"));
		System.out.println(isTestCaseRunnable(x,"SMOKE"));
		//Xls_Reader x1 = new Xls_Reader(System.getProperty("user.dir")+"\\src\\com\\AA\\xls\\AllegiantAirCC.xlsx");
		//System.out.println(isTestCaseRunnable(x1,"SMOKE"));
	}
	
	// returns true if runmode of the test is equal to Y
	public static boolean isTestCaseRunnable(Xls_Reader xls, String testCaseName){
		boolean isExecutable=false;
				//System.out.println(xls.getRowCount("TestNames"));
		for(int i=2; i<= xls.getRowCount("TestNames") ; i++){
			//String tcid=xls.getCellData("Test Cases", "TCID", i);
			//String runmode=xls.getCellData("Test Cases", "Runmode", i);
			//System.out.println(tcid +" -- "+ runmode);
		
			if(xls.getCellData("TestNames", "Test Name", i).equalsIgnoreCase(testCaseName)){
				if(xls.getCellData("TestNames", "Run Mode", i).equalsIgnoreCase("Y")){
					isExecutable= true;
				}else{
					isExecutable= false;
				}
			}
		}
		
		//xls = null;
		return isExecutable;
		
	}

}
