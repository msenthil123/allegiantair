package test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

public class getDate {

	
	public static void main(String[] args) {
		System.out.println( getDatefromToday(9));
		
	}
	
	/**
	 * @param args
	 * @return 
	 */
	
	
	 public static  String getDatefromToday(int  days) {
		 
		   DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		   	 
		   //get current date time with Calendar()
		   Calendar cal = Calendar.getInstance();
		   cal.add(Calendar.DATE, days);
		   //System.out.println(dateFormat.format(cal.getTime()));
		  
		   
		   return dateFormat.format(cal.getTime());
	 
	  }
}
