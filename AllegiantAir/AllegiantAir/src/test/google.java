package test;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class google {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		WebDriver driver = new FirefoxDriver();
		driver.get("http://google.co.in");
	
	}
	
	public static void takeScreenshot(WebDriver driver) throws IOException{
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	     // Now you can do whatever you need to do with it, for example copy somewhere
	        // FileUtils.copyFile(scrFile, new File("c:\\tmp\\screenshot.png"));
	        FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir")+"//src//com//AA//xls//screenshot.png"));
	}

}
