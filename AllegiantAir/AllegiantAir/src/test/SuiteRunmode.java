package test;

import com.AA.util.Xls_Reader;

public class SuiteRunmode {


	public static void main(String[] args) {
		System.out.println(System.getProperty("user.dir"));
		Xls_Reader x = new Xls_Reader(System.getProperty("user.dir")+"\\src\\com\\AA\\xls\\Suite.xlsx");
		System.out.println(isSuiteRunnable(x,"AlleginatAirWeb"));
		System.out.println(isSuiteRunnable(x,"AllegiantAirCC"));

		System.out.println(isSuiteRunnable(x,"AllegiantAirAIS"));

	}
	
	// finds if the test suite is runnable 
	public static boolean isSuiteRunnable(Xls_Reader xls , String suiteName){
		boolean isExecutable=false;
		//System.out.println(xls.getRowCount("App Name"));
		for(int i=2; i <= xls.getRowCount("App Name") ;i++ ){
			String suite = xls.getCellData("App Name", "Application Name", i);
			String runmode = xls.getCellData("App Name", "Run Mode", i);
		
			if(suite.equalsIgnoreCase(suiteName)){
			
				if(runmode.equalsIgnoreCase("Y")){
					isExecutable=true;
				}else{
					isExecutable=false;
				}
			}

		}
		xls=null; // release memory
		return isExecutable;
		
	}

}
