package test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

// import org.openqa.selenium.SearchContext;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.apache.commons.io.FileUtils;

//import com.sun.jna.platform.FileUtils;

public class FlightSearchPage
{

    static WebDriver driver;

    public static void main(String[] args) throws InterruptedException, IOException
    {
        driver= new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
       // driver.manage().window().maximize();

        driver.get("https://silo1.qat.allegiantair.com/");

        
        
        WebElement Sign_in_link = driver.findElement(By.xpath("//*[@id='mini-panel-g4_account_menu']/div/div/div/div/ul/li[3]/a"));
		Sign_in_link.click();
		
		Thread.sleep(1000);
		
		
		WebElement login_panel = driver.findElement(By.id("user-login"));
		
		//Enter username
		login_panel.findElement(By.id("edit-name")).sendKeys("iorn123@gmail.com");
		
		//Enter password
		login_panel.findElement(By.id("edit-pass")).sendKeys("iorn007");

		//Click signin btton
		login_panel.findElement(By.id("edit-submit")).click();		
		
		Thread.sleep(5000);
        
        
        // From
        driver.findElement(By.id("sfrom")).click();
        //Thread.sleep(2000);
      //  driver.findElement(By.id("sfrom")).sendKeys(Keys.DOWN);
     //  System.out.println(driver.findElement(By.id("sfrom")).getAttribute("value"));
      // String cmbVal;
     
     //  driver.findElement(By.id("sfrom")).sendKeys(Keys.DOWN);
    //   System.out.println(driver.findElement(By.id("sfrom")).getAttribute("value"));
       
     //  driver.findElement(By.id("sfrom")).sendKeys(Keys.DOWN);
      // System.out.println(driver.findElement(By.id("sfrom")).getAttribute("value"));
       
      // driver.findElement(By.id("sfrom")).sendKeys(Keys.DOWN);
   //    System.out.println(driver.findElement(By.id("sfrom")).getAttribute("value"));
       driver.findElement(By.id("suggest_from")).findElement(By.tagName("a")).click();

        // To
       driver.findElement(By.id("sto")).click();
       driver.findElement(By.id("suggest_to")).findElement(By.tagName("a")).click();

        // Date
        Thread.sleep(20000);
        driver.findElement(By.id("ddate")).click();
        // driver.findElement(By.className("ui-datepicker-calendar")).findElement(By.xpath("//a[contains(@class,'ui-state-default ui-state')]")).click();

        driver.findElement(By.className("ui-datepicker-calendar")).findElement(By.linkText("27")).click();
        driver.findElement(By.id("rdate")).click();

        // driver.findElement(By.className("ui-datepicker-calendar")).findElement(By.xpath("//a[contains(@class,'ui-state-default ui-state')]")).click();

        driver.findElement(By.className("ui-datepicker-calendar")).findElement(By.linkText("13")).click();

        driver.findElement(By.id("submit-search")).click();

       
        driver.switchTo().defaultContent();

        driver.switchTo().frame(0);
        
        
        
        
        

        // Select Flight tab

        driver.findElement(By.id("vertical-tabs")).findElement(By.linkText("Select Flights")).isEnabled();

        // System.out.println( "Getting Around Status " +
        // driver.findElement(By.id("vertical-tabs")).findElement(By.linkText("Getting Around")).isDisplayed());

        System.out.println("-----------------------------------------------------------------------------------");

        // Departure title, flight no and price information

        WebElement departure= driver.findElement(By.id("departing"));
        
        List<WebElement> allchoose = departure.findElements(By.className("choose"));
        
        System.out.println("Total Choose is : " + allchoose.size());
        
        for(WebElement Allchoose : allchoose)
        {
        	if(Allchoose.isDisplayed())
        	{
        		Allchoose.click();
        	}
        }
        

        System.out.println(departure);

        System.out.println(departure.findElement(By.tagName("h3")).getText());

        System.out.println(departure.findElement(By.className("flightNo")).getText());

        System.out.println(departure.findElement(By.xpath("//p[contains(@class,'singlePrice')]")).getText());

        departure.findElement(By.linkText("choose")).click();

       // trial.click();

        Thread.sleep(1000);

        WebElement returning= driver.findElement(By.id("returning"));

        returning.findElement(By.linkText("choose")).click();

        // to click Continue button

        driver.findElement(By.className("button-panel")).findElement(By.tagName("button")).click();

        Thread.sleep(5000);

        // Hotel

        // driver.findElement(By.className("button-panel")).findElement(By.tagName("button")).click(); // TO click continue button working fine

        try

        {

            System.out.println("Hotel page");

            driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/div[2]/div/div[3]/a")).click();

        }
        catch (Exception e)
        {

            System.out.println("Hotel page is not displayed");

        }

        Thread.sleep(5000);

        // Car page

        System.out.println("Car page");

        driver.findElement(By.xpath("//*[@id='no-car']"));

        driver.findElement(By.id("finalchoose")).findElement(By.linkText("Select")).click();

        driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div/div[3]/p[2]/a")).click(); // To click No Thanks option
                                                                                                                         // in Car page

        driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/div[3]/div/div[3]/div/button")).click(); // Continue button

        Thread.sleep(5000);

        // Special page

        driver.findElement(By.id("main-area")).findElement(By.className("continue")).click();

        Thread.sleep(5000);

        // Traveller page

        List<WebElement> allHeaders= driver.findElement(By.id("travellers")).findElements(By.tagName("form"));
        
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
     // Now you can do whatever you need to do with it, for example copy somewhere
        // FileUtils.copyFile(scrFile, new File("c:\\tmp\\screenshot.png"));
        FileUtils.copyFile(scrFile, new File("c:\\tmp\\screenshot.png"));
        System.out.println("This is the total numbers of headers" + allHeaders.size());

        String lastname= "ahmed";

        String gender= "Male";

        int tempinc = 0;
        for (WebElement header: allHeaders)

        {

            header.findElement(By.name("traveller[firstname]")).sendKeys("imthi");

            header.findElement(By.name("traveller[lastname]")).sendKeys(lastname);

           if (tempinc >0){
            header.findElement(By.name("traveller[dob]")).sendKeys("06/06/1986");
            
           }
           tempinc ++;
            WebElement Genders= header.findElement(By.className("rgroup"));

            List<WebElement> allGender= Genders.findElements(By.tagName("label"));

            for (WebElement WGender: allGender)

            {

                if (WGender.getText().equalsIgnoreCase(gender))

                {

                    WGender.click();

                }

            }

            lastname= "ahmed";

            gender= "Female";

        }

        driver.findElement(By.id("travellers")).findElement(By.className("continue")).click();

        Thread.sleep(2000);
        selectSeat("6E|7E","6E|7E", "Round Trip" );
               
        Thread.sleep(2000);
        //res_BagSelection();   
               
        Thread.sleep(20000);
        
        try
        {
            
        	driver.findElement(By.className("close_tripflex_message")).click();
        }
        catch (Exception e)
        {
        	System.out.println(e.getStackTrace());
        }
        
        driver.findElement(By.id("allegiant_models_payment_details_undefined_terms_accepted")).click();
        
        Thread.sleep(3000);
        Select selection_ct =new Select(driver.findElement(By.id("allegiant_models_payment_details_undefined_card_type")));
        selection_ct.selectByVisibleText("Visa Credit");
 	  driver.findElement(By.id("allegiant_models_payment_details_undefined_card_no")).sendKeys("4444-4444-4444-4448");
 	  
 	 
 	 Select selection_month =new Select(driver.findElement(By.id("allegiant_models_payment_details_undefined_expires_month")));
	   selection_month.selectByVisibleText("03 March");
	     
    try{
	 	 Select selection_year =new Select(driver.findElement(By.id("allegiant_models_payment_details_undefined_expires_year")));
	   selection_year.selectByVisibleText("2015");
    }catch(Exception e){
    	
    }
	   
	   driver.findElement(By.id("allegiant_models_payment_details_undefined_ccv")).sendKeys("123");
	   
	   driver.findElement(By.id("allegiant_models_payment_details_undefined_name_on_card")).sendKeys("Senthil kumar");
	   
	   driver.findElement(By.id("allegiant_models_payment_details_undefined_email_conf")).sendKeys("iorn123@gmail.com");
	   
	   
	   driver.findElement(By.className("purchase")).click();
    
    }

    public static void selectSeat(String depSeatstr, String retSeatSelect, String tripType) throws InterruptedException
    {
      
    	//String depSeatstr = "6E|7E";
    			
    WebElement rowObj;
   	String[] depSeatArr = depSeatstr.split("\\|");
   	WebElement seatingPlan= driver.findElement(By.xpath("//div[@class='plan allegiant_seat_plan']")).findElement(By.tagName("table"));
   	 
   	 for(int i=0;i<depSeatArr.length;i++)
     	 {
   		     try
               {
   			      depSeatArr[i]= depSeatArr[i].toUpperCase();
                  WebElement seat= seatingPlan.findElement(By.xpath("//a[contains(@class,'_seat_" + depSeatArr[i] + "')]"));
                  seat.click();
                  System.out.println("Seat no " + depSeatArr[i] + " is selected");
                }
            catch (Exception e)
                {
                  WebElement firstAvailableSeat= seatingPlan.findElement(By.xpath("//a[contains(@class,'seat_link')]"));
                  String seat= firstAvailableSeat.getAttribute("class");
                  String seatNo= seat.split("models_seat_")[1];
                  firstAvailableSeat.click();
                  System.out.println("Given seat no is  already selected. So the next available seat " + seatNo + " is selcted.");

                }
   		
   	 //Attempt to close alert
             try
               {
            
        	     driver.findElement(By.xpath("//button[@class='close_popup']")).click();
               }
            catch (Exception e)
               {
        	     System.out.println(e.getStackTrace());
               }          
   		     
     	 }    	    
       
     driver.findElement(By.className("continue")).click();
     Thread.sleep(2000);
 
     try
        {
     
 	     driver.findElement(By.className("yes_no_seats")).click();
        }
    catch (Exception e)
        {
      	System.out.println(e.getStackTrace());
        }
    }
    
public static void res_BagSelection(String Bag){
	
	String inputStr = "No|2|Yes||1|No|No";
	WebElement rowObj;
	 String[] firstname = inputStr.split("\\|\\|");
	 
	 for(int i=0;i<firstname.length;i++)
	 {
		 String[] selectVals = firstname[i].split("\\|");
		
			 System.out.println("//form[contains(@class,'adult_"+i+" form_binder')]");
			 rowObj= driver.findElement(By.xpath("//form[contains(@class,'adult_"+i+" form_binder')]"));
			 
			 
			 Select selection_bag =new Select(rowObj.findElement(By.name("traveller[bin_bags]")));
			 selectBypartText(selection_bag, selectVals[0]);
			 
			 try{
			 driver.findElement(By.id("ok_zero_bags")).click();
			 }catch(Exception e){
				 
			 }
			 
			 Select selection_chk =new Select(rowObj.findElement(By.name("traveller[checked_bags]")));
			  selectBypartText(selection_chk, selectVals[1] + " Bags");

			 
			 Select selection_prio =new Select(rowObj.findElement(By.name("traveller[priority_boarding_selected]")));
			 selectBypartText(selection_prio, selectVals[2]);
			 
			 
		 
		// for (int j=0;j<)
	 }
	//ok_zero_bags
	
	
	
		
	}

public static void selectBypartText(Select listObj, String partText){
	
	 List<WebElement> list = listObj.getOptions();
	 for (WebElement option : list) {
	 String fullText = option.getText();
	 if (fullText.contains(partText)) {
		 listObj.selectByVisibleText(fullText);
	 }
	 }
	
}
}
