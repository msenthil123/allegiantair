package test;

import com.AA.util.Xls_Reader;

public class UpdateResult_DataSet {
	
	public static void main(String[] args) {

		Xls_Reader x = new Xls_Reader(System.getProperty("user.dir")+"\\src\\com\\AA\\xls\\AlleginatAirWeb.xlsx");		
		reportDataSetResult(x,"BAT",2,"Pass");
		reportDataSetResult(x,"BAT",3,"Fail");
		
	}

    // update results for a particular data set	
	public static void reportDataSetResult(Xls_Reader xls, String testCaseName, int rowNum,String result){	
		xls.setCellData(testCaseName, "Results", rowNum, result);
	}
}
