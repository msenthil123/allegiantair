package com.AA.base;

import java.io.FileInputStream;
//import java.io.FileNotFoundException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.AA.util.Xls_Reader;

public class TestBase {
	public static Logger APP_LOGS=null;
	public static Properties CONFIG=null;
	public static Properties OR=null;
	public static Xls_Reader AAwebXls=null;
	public static Xls_Reader AAccxls=null;
	public static Xls_Reader AAaisxls=null;
	public static Xls_Reader suiteXls=null;
	public static boolean isInitialized = false;
	
	
	// initializing the Tests
	public void initialize() throws Exception{
		
		if(!isInitialized){
		// logs
		APP_LOGS = Logger.getLogger("devpinoyLogger");
		// config
		APP_LOGS.debug("Loading Property files");
		CONFIG = new Properties();
		FileInputStream ip = new FileInputStream(System.getProperty("user.dir")+"//src//com//AA//config/config.properties");
		CONFIG.load(ip);
			
		OR = new Properties();
		ip = new FileInputStream(System.getProperty("user.dir")+"//src//com//AA//config/OR.properties");
		OR.load(ip);
		APP_LOGS.debug("Loaded Property files successfully");
		APP_LOGS.debug("Loading XLS Files");

		// xls file
		AAwebXls = new Xls_Reader(System.getProperty("user.dir")+"//src//com//AA//xls//AlleginatAirWeb.xlsx");
		AAccxls = new Xls_Reader(System.getProperty("user.dir")+"//src//com//AA//xls//AllegiantAirCC.xlsx");
		AAaisxls = new Xls_Reader(System.getProperty("user.dir")+"//src//com//AA//xls//AllegiantAirAIS.xlsx");
		suiteXls = new Xls_Reader(System.getProperty("user.dir")+"//src//com//AA//xls//Suite.xlsx");
		APP_LOGS.debug("Loaded XLS Files successfully");		
		
		// selenium RC/ Webdriver
		//System.out.println("done");
		
		isInitialized = true;
		
		}
		
		
	}

}
