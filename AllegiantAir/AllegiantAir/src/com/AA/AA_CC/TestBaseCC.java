package com.AA.AA_CC;

import org.testng.SkipException;
import org.testng.annotations.BeforeSuite;

import com.AA.base.TestBase;
import com.AA.util.TestUtil;

public class TestBaseCC extends TestBase {

	// check if the suite ex has to be skiped
			@BeforeSuite
			public void checkSuiteSkip() throws Exception{
				initialize();
				APP_LOGS.debug("Checking Runmode of AllegiantAirCC");
				if(!TestUtil.isSuiteRunnable(suiteXls, "AllegiantAirCC")){
					APP_LOGS.debug("Skipped AllegiantAirCC as the runmode was set to NO");
					throw new SkipException("RUnmode of AllegiantAirCC set to no. So Skipping all tests in AllegiantAirCC");
				}
				
			}
	
}
