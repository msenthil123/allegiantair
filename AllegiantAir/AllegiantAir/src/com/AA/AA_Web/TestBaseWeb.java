package com.AA.AA_Web;

import org.testng.SkipException;
import org.testng.annotations.BeforeSuite;

import com.AA.base.TestBase;
import com.AA.util.TestUtil;

public class TestBaseWeb extends TestBase {
	public static int screenshotCnt=0;
	// check if the suite ex has to be skiped
		@BeforeSuite
		public void checkSuiteSkip() throws Exception{
			initialize();
			APP_LOGS.debug("Checking Runmode of AlleginatAirWeb");
			if(!TestUtil.isSuiteRunnable(suiteXls, "AlleginatAirWeb")){
				APP_LOGS.debug("Skipped AlleginatAirWeb as the runmode was set to NO");
				throw new SkipException("RUnmode of AlleginatAirWeb set to no. So Skipping all tests in AlleginatAirWeb");
			}
			
		}
	
}
