package com.AA.AA_Web;

import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.AA.util.TestUtil;

public class SMOKE_Web extends TestBaseWeb{
	
	
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(AAwebXls,"SMOKE")){
			APP_LOGS.debug("Skipping Test Case SMOKE as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case SMOKE as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		//runmodes=TestUtil.getDataSetRunmodes(suiteAxls, this.getClass().getSimpleName());
	}
	
	
	@Test(dataProvider="getTestData")
	public void SMOKE_Web_bs(String runMode, String descriptTion, String reSult, 
			 String userName, String passWord, String displayName,
			 String bookingType, String depCity, String destCity,
			 String tripType,  String adultsCount,String childCount,
			 String detpDate, String retDate){
		

		
		APP_LOGS.debug(" Executing SMOKE");
		APP_LOGS.debug(runMode +" -- "+descriptTion +" -- "+reSult +" -- "+userName + " -- " + detpDate 
				      +" -- " + retDate + " -- " + adultsCount + " -- " + childCount);
		
		
	}
	

	@DataProvider
	public Object[][] getTestData(){
		return TestUtil.getData(AAwebXls, "SMOKE");
	}

}
