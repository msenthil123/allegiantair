package com.AA.AA_Web.Reusable_AA_Web;


import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.AA.util.TestUtil;

public class res_FlightSelection extends res_invokeApp{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public static void verifyFlightSelectionPageinfo(String trip) throws InterruptedException, IOException{
		
		
		 Thread.sleep(5000);

		 System.out.println("Selectin flight");
        driver.switchTo().defaultContent();
        driver.switchTo().frame(0);
        try{
        //Select Flight tab
        if(driver.findElement(By.id("vertical-tabs")).findElement(By.linkText("Select Flights")).isEnabled())
        {
        
        //Departure title, flight no and price information
       // WebElement departure= driver.findElement(By.id("departing"));
       // System.out.println( departure.findElement(By.tagName("h3")).getText());
        	 WebElement departure= driver.findElement(By.id("departing"));
             System.out.println( departure.findElement(By.tagName("h3")).getText());
             
             //Get flight number and price       
             WebElement dept_Flight_no = departure.findElement(By.className("flight_no"));
             System.out.print("\nFlight No : " + dept_Flight_no.getText());
             System.out.println("-" + departure.findElement(By.className("stops")).getText());
             WebElement dep_Flight_price = departure.findElement(By.xpath("//p[contains(@class,'singlePrice')]"));
             System.out.println("Price --> " + dep_Flight_price.getText());
             
             //Select Departure Flight
             //WebElement dept_Choose_button = departure.findElement(By.className("choose"));
            // dept_Choose_button.click();
             List<WebElement> allchoose = departure.findElements(By.className("choose"));
             
             System.out.println("Total Choose is : " + allchoose.size());
             
             for(WebElement Allchoose : allchoose)
             {
             	if(Allchoose.isDisplayed())
             	{
             		Allchoose.click();
                        System.out.println("Departure flight is selected successfully. Flight no is " + dept_Flight_no.getText());
             	}
             }

        System.out.println("***********************************************************");
        Thread.sleep(1000);
        
        if(trip.equals("Round Trip")){
            Thread.sleep(500);
            
       //Departure title, flight no and price information
        WebElement returning = driver.findElement(By.id("returning"));
        System.out.println( returning.findElement(By.tagName("h3")).getText());
        System.out.println("***********************************************************");
       //Get flight number and price 
        WebElement Flightno = returning.findElement(By.className("flightNo"));
        System.out.println(Flightno.getText());
        WebElement ret_Flight_no = Flightno.findElement(By.className("flight_no"));
        System.out.print("\nFlight No : " + ret_Flight_no.getText());
        System.out.println("-" + Flightno.findElement(By.className("stops")).getText());
        WebElement ret_Flight_price = returning.findElement(By.xpath("//p[contains(@class,'singlePrice')]"));
        System.out.println("Price --> " + ret_Flight_price.getText());
        
        List<WebElement> allretchoose = departure.findElements(By.className("choose"));
        
        System.out.println("Total Choose is : " + allretchoose.size());
        
        for(WebElement Allretchoose : allretchoose)
        {
        	if(Allretchoose.isDisplayed())
        	{
        		Allretchoose.click();
                         System.out.println("Departure flight is selected successfully.  " );
        	}
        }
       
       //Click Return Flight
       //returning.findElement(By.linkText("choose")).click();
      // System.out.println("***********************************************************");

       }
       

        //Flight summary
        System.out.println("\n" + driver.findElement(By.id("summary")).getText());
       
       
       //Click Continue Button
       driver.findElement(By.className("button-panel")).findElement(By.tagName("Button")).click();
        }
        else
        {
        	System.out.println("Search Flight Button is not enable in vertical tab");
        }
       
        }catch(Exception e)
        {
        	TestUtil.takeScreenshot(driver);
        }
		
	}

}
