package com.AA.AA_Web.Reusable_AA_Web;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.AA.AA_Web.BAT_Web;
import com.AA.util.TestUtil;

public class res_PaymentPage extends res_invokeApp {

	/**
	 * @param args
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public static String res_PaymentInfoFilling(String emailAdd) throws InterruptedException, IOException {
		String conf = "";
		  //Close trip flex message
		
		  try
	        {
	           
	        	WebElement messageBox = driver.findElement(By.className("message-inner"));
	        	messageBox.findElement(By.className("button")).click();
	        	System.out.println("closing the message box");
	        }
	        catch (Exception e)
	        {
	        	System.out.println(e.getStackTrace());
	        	System.out.println("Message Box not closed");
	        }
	        
	try{	
		//Accept Terms and conditions
   	     WebElement tacBox = driver.findElement(By.xpath("//div[contains(@class,'tandc')]"));
		 WebElement termAcceptText =  tacBox.findElement(By.xpath("//div[contains(@class,'allegiant_models_payment_details_undefined_terms_accepted')]"));
		 termAcceptText.click();
		 System.out.println("first click");
		// Thread.sleep(5000);
		// termAcceptText.click();
		// System.out.println("second click");
		// Thread.sleep(5000);
		// termAcceptText.findElement(By.xpath("//input[contains(@type,'checkbox')]")).click();
		// Thread.sleep(10000);
		 
		//driver.findElement(By.id("allegiant_models_payment_details_undefined_terms_accepted")).click();
        
        Thread.sleep(3000);
        Select selection_ct =new Select(driver.findElement(By.id("allegiant_models_payment_details_undefined_card_type")));
        selection_ct.selectByVisibleText("Visa Credit");
 	    driver.findElement(By.id("allegiant_models_payment_details_undefined_card_no")).sendKeys("4444-4444-4444-4448");
 	  
 	 
 	    Select selection_month =new Select(driver.findElement(By.id("allegiant_models_payment_details_undefined_expires_month")));
	    selection_month.selectByVisibleText("03 March");
	}catch(Exception e){
		common_Functions.logErrorMessage("Error in confirmation page");
	}
	     
         try{
	        	 Select selection_year =new Select(driver.findElement(By.id("allegiant_models_payment_details_undefined_expires_year")));
	             selection_year.selectByVisibleText("2015");
            
	   
	   driver.findElement(By.id("allegiant_models_payment_details_undefined_ccv")).sendKeys("123");
	   
	   driver.findElement(By.id("allegiant_models_payment_details_undefined_name_on_card")).sendKeys("Senthil kumar");
	   
	   //driver.findElement(By.id("allegiant_models_payment_details_undefined_email_conf")).sendKeys("iorn123@gmail.com");
	   driver.findElement(By.id("allegiant_models_payment_details_undefined_email_conf")).sendKeys("sudhan@gmail.com");
	   
	   
	   driver.findElement(By.className("purchase")).click();
	   
         }catch(Exception e){
        		common_Functions.logErrorMessage("Error in confirmation page");
         }
	   
	   Thread.sleep(30000);
          try{	   

	    conf = driver.findElement(By.xpath("/html/body/div[2]/div/div[3]/div/p/em")).getText();
      System.out.println("Confirmation number is " + conf);}
         catch(Exception e)   {
        	 Thread.sleep(30000);
        	 try{	   

        		    conf = driver.findElement(By.xpath("/html/body/div[2]/div/div[3]/div/p/em")).getText();
        	      System.out.println("Confirmation number is " + conf);}
        	         catch(Exception ex)   {
        	        	 Thread.sleep(30000);
        	        	 try{	   
        	        		 driver.findElement(By.id("allegiant_models_payment_details_undefined_card_no")).sendKeys("4444-4444-4444-4448");
        	        		 driver.findElement(By.id("allegiant_models_payment_details_undefined_ccv")).sendKeys("123");
        	        		 driver.findElement(By.className("purchase")).click();
        	        		 System.out.println("second time details entered");
        	        		 Thread.sleep(20000);
        	        		    conf = driver.findElement(By.xpath("/html/body/div[2]/div/div[3]/div/p/em")).getText();
        	        	      System.out.println("Confirmation number is " + conf);}
        	        	         catch(Exception exc)   {
        	        	                   System.out.println("Confirmation number is not generated. Issue in payment page");
        	        	                   TestUtil.takeScreenshot(driver);
        	        	            }
        	            }
            }

	   
	   try{
	         //driver.findElement(By.xpath("/html/body/div[4]/div/a")).click();
	         driver.findElement(By.xpath("//span[contains(@class,'ui-icon-close')]")).click();
	   }catch(Exception e){
		   
		   //Thread.sleep(10000);
		  // driver.findElement(By.xpath("/html/body/div[4]/div/a/span")).click();
		 //  return conf;
	   }
	   if (conf == "")
	   {
		   
		   System.out.println("Confirmation number is not generated. Issue in payment page");
		   TestUtil.takeScreenshot(driver);
	   }
	   return conf;
	   
	   
	}

}
