package com.AA.AA_Web.Reusable_AA_Web;

//import org.apache.bcel.generic.Select;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.AA.util.ErrorUtil;
import com.AA.util.TestUtil;

public class res_SearchFlight extends res_invokeApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public static boolean res_searchflight(String depCity, String destCity, String tripType, String depDate, String retDate, String adults, String child, String child_month, String child_day, String child_year) throws InterruptedException{
		
		try{
			common_Functions.logMessage("Searching for Flight");
			common_Functions.logMessage("Waiting to be loaded");
			Thread.sleep(10000);
			
			 common_Functions.selectCityValue(driver, depCity, "sfrom", "Departure City Dropdown");
			
			 common_Functions.selectCityValue(driver, destCity, "sto", "Destination City Dropdown");
		     Thread.sleep(15000);
		//Select Trip type
        WebElement Selected_trip = driver.findElement(By.xpath("//*[@id='trip']"));
		//Selected_trip.click();
        common_Functions.logMessage("5");
		Selected_trip.sendKeys(tripType);
		common_Functions.logMessage("Trip type is selected");
        Thread.sleep(2000);  

        
        //depDate = "23";
       // retDate = "25";
        //Select Departure Date
        driver.findElement(By.id("ddate")).click();
        //driver.findElement(By.className("ui-datepicker-calendar")).findElement(By.xpath("//a[contains(@class,'ui-state-default ui-state')]")).click();
        driver.findElement(By.className("ui-datepicker-calendar")).findElement(By.xpath("//a[text()='" + depDate + "']")).click();
        common_Functions.logMessage("Departure date is selected");
        
       
        
        System.out.println(tripType);
        //Select Return Date
        if(tripType.equals("Round Trip"))
        {
        	System.out.println("selecting ret date");
        	driver.findElement(By.id("rdate")).click();
        	//driver.findElement(By.className("ui-datepicker-calendar")).findElement(By.xpath("//a[contains(@class,'ui-state-default ui-state')]")).click();
        	driver.findElement(By.className("ui-datepicker-calendar")).findElement(By.xpath("//a[text()='" + retDate + "']")).click();
        	common_Functions.logMessage("Return date is selected");
        	
        }
        
        
		//Select adults count from Adults combo box
        driver.findElement(By.xpath("//select[@id='adults']")).click();
        driver.findElement(By.xpath("//select[@id='adults']")).sendKeys(adults);
        common_Functions.logMessage("adults count is selected");
		
		  
        
       
		//Select child count Child combo box
        if (child != "0" | child != null)
        {
        	common_Functions.logMessage("Selecting child info");
        	Select child_combobox = new Select(driver.findElement(By.xpath("//select[@id='children']")));
        	child_combobox.selectByVisibleText(child);
        
        	Thread.sleep(1000); 
        	
        	String[] month = child_month.split("[|]");      	  
        	String[] day = child_day.split("[|]");     
        	String[] year = child_year.split("[|]");  

        	int child_count = Integer.parseInt(child); 
             System.out.println(child_count);
        //Fill Child DOB details in combobox	
        	for(int i = 1;i<=child_count;i++)
        	{
            
        		System.out.println("child_"+ i);
        	String monthid = "child_"+i+"_month";
        	   Select selection_month =new Select(driver.findElement(By.id(monthid)));
        	   selection_month.selectByVisibleText(month[i-1]);
       		System.out.println("childday_"+ i);

        	String dayid = "child_"+i+"_day";
        	   Select selection_day =new Select(driver.findElement(By.id(dayid)));
        	   selection_day.selectByVisibleText(day[i-1]); 
          		System.out.println("childyear_"+ i);

        	String yearid = "child_"+i+"_year";
        	   Select selection_year =new Select(driver.findElement(By.id(yearid)));
        	   selection_year.selectByVisibleText(year[i-1]);  
          		System.out.println(" done for child_"+ i);

        	}
        }
        
		
		//Click Search button
        if (driver.findElement(By.xpath("//input[@id='submit-search']")).isEnabled())
        {
        	driver.findElement(By.id("submit-search")).click();
        	common_Functions.logMessage("Searching criteria submitted.");
        	Thread.sleep(5000);
        }
        else
        {
        	System.out.println("Submit button is not enable");
	    }
        
       // driver.quit();
		return true;
		} catch (Exception t)
		{
			// code to report the error in testng
			ErrorUtil.addVerificationFailure(t);
			System.out.println("Exception in flight search page");
			System.err.println(t.getStackTrace());
			t.printStackTrace();
			
			//driver.quit();
			return false;
                     
		}
	}

	public static void Select_date(String Date_to_Select)
	{

	 //Date_to_Select should be passed dd/mm/yyyy
	 
	 String[] date_to_select = new String[3];
	 date_to_select = Date_to_Select.split("[/]");
	    
	    String Date = date_to_select[0];
	    String Month = date_to_select[1];
	    String Year = date_to_select[2];
	    
	    String Month_Year = Month +" "+ Year;
	    
	    
	    
	    driver.findElement(By.id("ddate")).click();
	    WebElement Date_Picker =  driver.findElement(By.xpath("//div[@id='ui-datepicker-div']"));
	    String Calender_title = Date_Picker.findElement(By.xpath("//div[contains(@class,'group-first')]")).findElement(By.className("ui-datepicker-title")).getText();
	        
	    while(!Calender_title.trim().equals(Month_Year))
	     {
	     driver.findElement(By.xpath("//a[@title='Next']")).click();
	     Calender_title = Date_Picker.findElement(By.xpath("//div[contains(@class,'group-first')]")).findElement(By.className("ui-datepicker-title")).getText();
	     }
	    
	       
	    WebElement Date_Picker_table =  Date_Picker.findElement(By.xpath("//div[contains(@class,'group-first')]")).findElement(By.tagName("table"));
	    try
	    {
	    Date_Picker_table.findElement(By.linkText(Date)).click();
	    }
	    catch(Exception e)
	    {
	        WebElement Date_Available = Date_Picker_table.findElement(By.xpath("//a[contains(@class,'ui-state-default')]"));
	        System.out.println(" Departure date is disabled hence new departure date is selected as " +  Date_Picker_table.findElement(By.xpath("//a[contains(@class,'ui-state-default')]")).getText());
	        Date_Available.click();
	    }
	 
	  }
		
		//Click Search button
		
		
	}


