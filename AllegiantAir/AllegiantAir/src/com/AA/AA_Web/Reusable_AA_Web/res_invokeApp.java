package com.AA.AA_Web.Reusable_AA_Web;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class res_invokeApp {

	
	 public static WebDriver driver;

	
	public static void res_Invokeapplication(String browserName){
		
		  
	      if (browserName == "FireFox")
	      {
		     driver= new FirefoxDriver();
	      }else
	      {
	    	  driver= new InternetExplorerDriver();
	}
	         driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	         driver.manage().deleteAllCookies();
	        driver.manage().window().maximize();
                System.out.println("Firefox is invoked");
                
              // String urlVal = "http://www2.int.allegiantair.com/";
              String urlVal = "http://silo1.int.allegiantair.com/";
	         driver.get(urlVal);
                System.out.println("Navigating to the URL "+ urlVal );
	     
		
	}

}
