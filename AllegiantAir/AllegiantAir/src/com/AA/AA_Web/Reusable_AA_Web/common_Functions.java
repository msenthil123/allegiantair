package com.AA.AA_Web.Reusable_AA_Web;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.sun.jna.Structure.ByValue;

public class common_Functions {

	/**
	 * @param args
	 */
	public static void textBoxInput(WebElement tbObject, String tbName, String tbValue) {
		// TODO Auto-generated method stub
		
	try{
		tbObject.sendKeys(tbValue);
		logMessage(tbValue + " is entered into the " + tbName + " Textbox field.");
		
	}catch(Exception e){
		logErrorMessage(tbValue + " is not entered into the " + tbName + " Textbox field.");
		logErrorMessage(e.getLocalizedMessage());
		
	}				

	}
	
	public static void logMessage(String messageToLog){
		try{
			System.out.println(messageToLog);
			
		}catch(Exception e){
			
		}
	}
	
	
	public static void logErrorMessage(String messageToLog){
			try{
				System.out.println(messageToLog);
				
			}catch(Exception e){
				
			}
	}
	
	
	/**
	 * This method identifies the object using "id" value
	 * <p>
	 * This method always returns the WebElement (i.e Object) if it is available.
	 * If the object is not available it returns null.
	 * @param  driver  (WebDriver) WebDriver object on which we need to search the element
	 * @param  idVal    (String) This is value of the "id" attribute of the object
	 * @param  objName (String) This is name of object & it will be used to log messages               
	 * @return          If the object is identified it return the object as WebElement
	 *                  else it will return null
	 * 
	 */
	public static WebElement findObjectById(WebDriver driver, String idVal, String objName){
		//Identifies the element & if its available it returns it
		try{
		WebElement webObj = driver.findElement(By.id(idVal));
		return webObj;
		}catch (Exception ex)
		{
			logErrorMessage("The Object " + objName + " is not available.");
			return null;
		}
		
	
	}
	
	/**
	 * This method identifies the object using "xpath" value
	 * <p>
	 * This method always returns the WebElement (i.e Object) if it is available.
	 * If the object is not available it returns null.
	 * @param  driver  (WebDriver) WebDriver object on which we need to search the element
	 * @param  xpathVal    (String) This is value of the "id" attribute of the object
	 * @param  objName (String) This is name of object & it will be used to log messages               
	 * @return          If the object is identified it return the object as WebElement
	 *                  else it will return null
	 * 
	 */
	public static WebElement findObjectByxPath(WebDriver driver, String xpathVal, String objName){
		//Identifies the element & if its available it returns it
		try{
		WebElement webObj = driver.findElement(By.xpath(xpathVal));
		return webObj;
		}catch (Exception ex)
		{
			logErrorMessage("The Object " + objName + " is not available.");
			return null;
		}
		
	
	}
	
	
	public static void checkEnableStatus(WebDriver driver, String attr, String attrVal, String objName, String... tagName){
		try{
			WebElement webObj = findObject(driver, attr, attrVal, objName, tagName);
			if (webObj.isEnabled())
			{
				System.out.println(objName + " is enabled.");
			}
			else
			{
				System.out.println(objName + " is not enabled.");
			}
		}catch(Exception e){
			System.out.println(objName + " is not available." );
		}
		
	}
	
	
	/**
	 * This method identifies the object using "class" value
	 * <p>
	 * This method always returns the WebElement (i.e Object) if it is available.
	 * If the object is not available it returns null.
	 * @param  driver  (WebDriver) WebDriver object on which we need to search the element
	 * @param  className   (String) This is value of the "class name" attribute of the object
	 * @param  objName (String) This is name of object & it will be used to log messages               
	 * @return          If the object is identified it return the object as WebElement
	 *                  else it will return null
	 * 
	 */
	public static WebElement findObjectByclass(WebDriver driver,String className, String objName)
	{
		try{
			WebElement webObj = driver.findElement(By.className(className));
			return webObj;
			}catch (Exception ex)
			{
				logErrorMessage("The Object " + objName + " is not available.");
				return null;
			}
			
	}
	
	public static WebElement findObjectByvalue(WebDriver driver, String attrVal,String objName, String... tagName){
		try{
			WebElement webObj = driver.findElement(By.xpath("//"+tagName +  "[contains(value,'"+ attrVal +"')]"));
			return webObj;
			}catch (Exception ex)
			{
				logErrorMessage("The Object " + objName + " is not available.");
				return null;
			}
	}
	
	/**
	 * This method identifies appropriate method for object identification. 
	 * For example , if you want to identify the object with "id",it will call
	 * findObjectById method to identify the object.
	 * <p>
	 * This method always returns the WebElement (i.e Object) if it is available.
	 * If the object is not available it returns null.
	 * @param  driver  (WebDriver) WebDriver object on which we need to search the element
	 * @param  attr    (String) This is the attribute value using which we will identify the object.
	 *                 Possible values are "id", "class", "name","xpath", "class_contains"
	 * @param  attrVal (String) This is the value of the attribute                 
	 * @param  objName (String) This is name of object & it will be used to log messages               
	 * @return          If the object is identified it return the object as WebElement
	 *                  else it will return null
	 * 
	 */
	public static WebElement findObject(WebDriver driver, String attr, String attrVal, String objName, String... tagName){
	    
		switch(attr){
		//identifies object by id value
		case "id": return findObjectById(driver, attrVal, objName); 
		
		case "name": return findObjectByName(driver, attrVal, objName); 
		
		//identifies object by id value
		case "xpath": return findObjectByxPath(driver, attrVal, objName); 
		
		case "class": return findObjectByclass(driver, attrVal, objName); 
		
		case "value": return findObjectByvalue(driver, attrVal, objName, tagName); 

						
		default: logErrorMessage("The given attribute value " + attr + " may be invalid or it is not yet programmed. Please verify common_Functions.findObject method");
			     return null;
		}
		
		
		
	}
	
	
	/**
	 * This method is used to enter value in a Textbox. 
	 * <p>
	 * This will identify the Textbox using the give identification parameters (attr,attrVal & tagName)
	 * If the textbox is identified succussfully it will enter the given value in the textbox. Else it will log an error message
	 * @param  driver  (WebDriver) WebDriver object on which we need to search the element
	 * @param  attr    (String) This is the attribute value using which we will identify the object.
	 *                 Possible values are "id", "class", "name","xpath", "class_contains"
	 * @param  attrVal (String) This is the value of the attribute                 
	 * @param  objName (String) This is name of object & it will be used to log messages               
	 * @param  ValuetoEnter (String) This value will be entered into the textbox
	 * @param  tagName   (String) tagName of the html attribute. This parameter will be application only if the "attr" is "class_contains".
	 *                            For other attr its optional.
	 */
	public static void enterTextBoxValue(WebDriver driver, String attr, String attrVal, String objName, String ValuetoEnter, String... tagName ){
		//identify the object
		WebElement webObj = findObject(driver, attr, attrVal, objName, tagName);
		
		if (webObj != null){
			try{
				webObj.sendKeys(ValuetoEnter);
			     logMessage(ValuetoEnter + " is entered into " + objName );
		}catch (Exception e){
			logErrorMessage("There is an Exception while entering " + ValuetoEnter + " in the " + objName);
		}
		}
		
	}
		
	/**
	 * This method identifies the object using "id" value
	 * <p>
	 * This method always returns the WebElement (i.e Object) if it is available.
	 * If the object is not available it returns null.
	 * @param  driver  (WebDriver) WebDriver object on which we need to search the element
	 * @param  idVal    (String) This is value of the "id" attribute of the object
	 * @param  objName (String) This is name of object & it will be used to log messages               
	 * @return          If the object is identified it return the object as WebElement
	 *                  else it will return null
	 * 
	 */
	public static WebElement findObjectByName(WebDriver driver, String idVal, String objName){
		//Identifies the element & if its available it returns it
		try{
		WebElement webObj = driver.findElement(By.name(idVal));
		return webObj;
		}catch (Exception ex)
		{
			logErrorMessage("The Object " + objName + " is not available.");
			return null;
		}
		
	
	}
	
		/**
		 * This method is used to enter value in a Textbox. 
		 * <p>
		 * This will identify the Textbox using the give identification parameters (attr,attrVal & tagName)
		 * If the textbox is identified succussfully it will enter the given value in the textbox. Else it will log an error message
		 * @param  driver  (WebDriver) WebDriver object on which we need to search the element
		 * @param  attr    (String) This is the attribute value using which we will identify the object.
		 *                 Possible values are "id", "class", "name","xpath", "class_contains"
		 * @param  attrVal (String) This is the value of the attribute                 
		 * @param  objName (String) This is name of object & it will be used to log messages               
		 * @param  ValuetoEnter (String) This value will be entered into the textbox
		 * @param  tagName   (String) tagName of the html attribute. This parameter will be application only if the "attr" is "class_contains".
		 *                            For other attr, it is optional.
		 */
		public static void clickOnWebElement(WebDriver driver, String attr, String attrVal, String objName,  String...tagName ){
			//identify the object
			WebElement webObj = findObject(driver, attr, attrVal, objName, tagName);
			
			if (webObj != null){
				try{
					webObj.click();
				     logMessage(objName + " is clicked." );
				   
			}catch (Exception e){
				logErrorMessage("There is an Exception while clicking " + objName );
			}
			}
		
	
	}
		
		
		/**
		 * This method is used to check any checkBox objects. 
		 * <p>
		 * This will identify the checkbox using the give identification parameters (attr,attrVal & tagName)
		 * If the checkbox is identified successfully it will check it. Else it will log an error message
		 * @param  driver  (WebDriver) WebDriver object on which we need to search the element
		 * @param  attr    (String) This is the attribute value using which we will identify the object.
		 *                 Possible values are "id", "class", "name","xpath", "class_contains"
		 * @param  attrVal (String) This is the value of the attribute                 
		 * @param  objName (String) This is name of object & it will be used to log messages               
		 * @param  tagName   (String) tagName of the html attribute. This parameter will be application only if the "attr" is "class_contains".
		 *                            For other attr, it is optional.
		 */
		public static void checkCheckBox(WebDriver driver, String attr, String attrVal, String objName,  String...tagName ){
			//identify the object
			WebElement chkBox = findObject(driver, attr, attrVal, objName, tagName);
			
			if (chkBox != null){
				try{
					chkBox.click();
				     logMessage(objName + "CheckBox is checked." );
			}catch (Exception e){
				logErrorMessage("There is an Exception while checking " + objName + " Checkbox");
			}
			}
		
	
	}
		/**
		 * This method is used to select City name in the Flight Search panel
		 * <p>
		 * This method will identify the city name dropdown and use keyboard actions to select correct city name
		 * @param  driver  (WebDriver) WebDriver object on which we need to search the element
		 * @param  cityname Cityname - It should be the Airport code for that city which will have 3 characters (Eg: LAS, FLL)
		 * @param  idVal (String)     This is the idvalue of the combobox 
		 *                                 For Departure city - "sfrom"
		 *                                 For Destincation city - "sto"               
		 * @param  objName (String) This is name of object & it will be used to log messages               
		 * 
		 */
		public static void selectCityValue(WebDriver driver, String cityname, String idVal, String objName) throws InterruptedException{
			
			 driver.findElement(By.id(idVal)).click();
		     Thread.sleep(2000);
		     driver.findElement(By.id(idVal)).sendKeys(Keys.DOWN);
		     String cmbVal =  driver.findElement(By.id(idVal)).getAttribute("value");
		     
		     int countLoop = 0;
		       while (! cmbVal.contains(" ("+ cityname +")"))
		       {
		    	   if (countLoop ==0)
		    	   {
		    		   driver.findElement(By.id(idVal)).sendKeys(Keys.UP); 
		    	   }
		    	   else
		    	   {
		    	   driver.findElement(By.id(idVal)).sendKeys(Keys.DOWN);
		    	   }
				   cmbVal =  driver.findElement(By.id(idVal)).getAttribute("value");  
				   countLoop++;
				   //System.out.println(cmbVal);
				   if (countLoop>100)
				   {
					   break;
				   }
		       }
		       driver.findElement(By.id(idVal)).sendKeys(Keys.TAB);
		       cmbVal =  driver.findElement(By.id(idVal)).getAttribute("value");  
		       if  (cmbVal.contains(" ("+ cityname +")")){
		    	  
		    	   logMessage(cmbVal + " is selected in " + objName + ".");
		       }else{
		    	   logErrorMessage(cityname + " is not selected in " + objName + ".");
		       }
		}
		
		
		/**
		 * This method is used to select value from dropdown or combobox. 
		 * <p>
		 * This will identify the combobox using the give identification parameters (attr,attrVal & tagName)
		 * If the combobox is identified succussfully it will select the given value from it. Else it will log an error message
		 * @param  driver  (WebDriver) WebDriver object on which we need to search the element
		 * @param  attr    (String) This is the attribute value using which we will identify the object.
		 *                 Possible values are "id", "class", "name","xpath", "class_contains"
		 * @param  attrVal (String) This is the value of the attribute                 
		 * @param  objName (String) This is name of object & it will be used to log messages               
		 * @param  ValuetoSelect (String) This value will be entered into the textbox
		 * @param  tagName   (String) tagName of the html attribute. This parameter will be application only if the "attr" is "class_contains".
		 *                            For other attr its optional.
		 */
		public static void selectDropdownValue(WebDriver driver, String attr, String attrVal, String objName, String ValuetoSelect, String... tagName ){
			
			WebElement dropDownObj = findObject(driver, attr, attrVal, objName, tagName);
			try{
				dropDownObj.sendKeys(ValuetoSelect);
				logMessage(ValuetoSelect + " is selected from the " + objName + " dropdown field.");
				
			}catch(Exception e){
				logErrorMessage(ValuetoSelect + " is not selected from the " + objName + " dropdown field.");
				logErrorMessage(e.getLocalizedMessage());
				
			}				
			
		}
		
		/**
		 * This method is used to verify value in a dropdown or combobox. 
		 * <p>
		 * This will identify the combobox using the given identification parameters (attr,attrVal & tagName)
		 * If the combobox is identified succussfully it will compare the 'expValue' actual value selected in the combobox 
		 * @param  driver  (WebDriver) WebDriver object on which we need to search the element
		 * @param  attr    (String) This is the attribute value using which we will identify the object.
		 *                 Possible values are "id", "class", "name","xpath", "class_contains"
		 * @param  attrVal (String) This is the value of the attribute                 
		 * @param  objName (String) This is name of object & it will be used to log messages               
		 * @param  expValue (String) This value will be entered into the textbox
		 * @param  tagName   (String) tagName of the html attribute. This parameter will be application only if the "attr" is "class_contains".
		 *                            For other attr its optional.
		 */
		public static void verifyDropdownValue(WebDriver driver, String attr, String attrVal, String objName, String expValue, String... tagName ){
			
			WebElement dropDownObj = findObject(driver, attr, attrVal, objName, tagName);
			try{
				//System.out.println(dropDownObj.getAttribute(arg0));
				if (dropDownObj.getText().equals(expValue))
				{
				 logMessage(expValue + " is dispalyed in the " + objName + " dropdown field.");
				}
				else{
					logErrorMessage(expValue + " is not selected from the " + objName + " dropdown field.");
				}
				
			}catch(Exception e){
				logErrorMessage(expValue + " is not selected from the " + objName + " dropdown field.");
				logErrorMessage(e.getLocalizedMessage());
				
			}				
			
		}
	
	
	
}
