package com.AA.AA_Web.Reusable_AA_Web;
import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.AA.util.ErrorUtil;

public class res_login extends res_invokeApp {


	
	public static boolean res_Login(String uname, String pwd, String dispName) {
	
		  //Click signin link
		//  common_Functions.clickOnWebElement(driver, "xpath", "//*[@id='mini-panel-g4_account_menu']/div/div/div/div/ul/li[3]/a", "Sign In Link");
		  
		//  common_Functions.clickOnWebElement(driver, "href", "/user-login", "Sign In Link");
		  System.out.println("test");
		  driver.findElement(By.linkText("Sign In")).click();
		  System.out.println("test");
		  // 
		  
		  //Enter user name
		  common_Functions.enterTextBoxValue(driver, "id", "edit-name", "Username TextBox", uname);
		  
		  //Enter Password
		  common_Functions.enterTextBoxValue(driver, "id", "edit-pass", "Password TextBox", pwd);
		  
		  //Click singin button
		  common_Functions.clickOnWebElement(driver, "id", "edit-submit", "Submit Button");
		  
		  return true;
	}

}
