package com.AA.AA_Web.Reusable_AA_Web;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class res_SeatSelection extends res_invokeApp{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	
	 public static void selectSeatMain(String depSeatstr, String retSeatSelect, String tripType) throws InterruptedException
	    {
	      
	    	//String depSeatstr = "6E|7E";
		 selectSeats(depSeatstr, "departing");
		 
		 if (tripType.equalsIgnoreCase("Round Trip")){
			 
			driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/div[6]/div/div/div/ul/li[2]/a")).click();
			                              
			// driver.findElement(By.xpath("//a[contains(@href, '#returning')]"));
			 Thread.sleep(3000);
			 //Select returning flights
			 selectSeats(retSeatSelect, "returning");
			 
		 }
	    	    
	       
	     driver.findElement(By.className("continue")).click();
	     Thread.sleep(2000);
	 
	     try{
	    	
	    	// WebElement innerMesg = driver.findElement(By.className("message-inner"));
	    	 driver.findElement(By.xpath("/html/body/div[19]/div[2]/div/p[2]/button")).click();
	    	  driver.findElement(By.className("continue")).click();
	 	     Thread.sleep(2000);
	    	 //message-inner
	     }catch(Exception e){
	    	 try{ 
	    	 driver.findElement(By.className("continue")).sendKeys(Keys.ENTER);
	    	 }catch(Exception ex){
	    		 
	    	 }
	 	     Thread.sleep(2000);
	    	 
	     }
	     
	     try
	        {
	     
	 	     driver.findElement(By.className("yes_no_seats")).click();
	        }
	    catch (Exception e)
	        {
	      	System.out.println(e.getStackTrace());
	        }
	    }
	 
	 public static void selectSeats(String Seatstr, String tableid){
		 
		    WebElement rowObj;
		   	String[] SeatstrArr = Seatstr.split("\\|");
		   	WebElement seatingPlan= driver.findElement(By.id(tableid));
		   	 
		   	 for(int i=0;i<SeatstrArr.length;i++)
		     	 {
		   		     try
		               {
		   		    	SeatstrArr[i]= SeatstrArr[i].toUpperCase();
		               // WebElement seat= seatingPlan.findElement(By.xpath("//a[contains(@class,'_seat_" + SeatstrArr[i] + "')]"));
		                  //seat.click();
		   		    	clickVisbleobj("//a[contains(@class,'_seat_" + SeatstrArr[i] + "')]");
		   		    	//#returning    (//a[contains(@href, '#returning")])
		                  System.out.println("Seat no " + SeatstrArr[i] + " is selected");
		                }
		            catch (Exception e)
		                {
		                  //WebElement firstAvailableSeat= seatingPlan.findElement(By.xpath("//a[contains(@class,'seat_link')]"));
		                 // String seat= firstAvailableSeat.getAttribute("class");
		                 // String seatNo= seat.split("models_seat_")[1];
		                 // firstAvailableSeat.click();
		                 // System.out.println("Given seat no is  already selected. So the next available seat " + seatNo + " is selcted.");
		            	common_Functions.logMessage("Seat no "+ SeatstrArr[i] +" is not available ");
		                }
		   		
		   	 //Attempt to close alert
		             try
		               {
		            
		        	     driver.findElement(By.xpath("//button[@class='close_popup']")).click();
		               }
		            catch (Exception e)
		               {
		        	     System.out.println(e.getStackTrace());
		               }          
		   		     
		     	 }   
		 
	 }
	 
	 public static void clickVisbleobj(String xPathval){
		 //String xPathval = "//a[contains(@class,'_seat_" + SeatstrArr[i] + "')]";
		 List<WebElement> allSeats= driver.findElements(By.xpath(xPathval));
		 for(WebElement allseatObj : allSeats)
         {
               if(allseatObj.isDisplayed())
                {
            	   allseatObj.click();
                 }
   
           }
		 
		 
		 
	 }

}
