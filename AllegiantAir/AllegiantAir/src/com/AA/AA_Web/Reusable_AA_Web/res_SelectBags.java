package com.AA.AA_Web.Reusable_AA_Web;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.AA.util.TestUtil;

public class res_SelectBags extends res_invokeApp{

	
	
	public static void res_BagSelection(String inputStr, String passType) throws InterruptedException{
		
		// inputStr = "No|2|Yes||1|No|No";
		WebElement rowObj;
		 String[] passSep = inputStr.split("\\|\\|");
		 boolean noBaginfoFlag = false;
		 System.out.println(inputStr);
		 System.out.println(passSep);
		 for(int i=0;i<passSep.length;i++)
		 {
			 int passNo = i+1;
             common_Functions.logErrorMessage("Selecting Bags for  " + passType + "" + passNo);
			 String[] selectVals = passSep[i].split("\\|");
			
			if (passType == "adult"){	
			 System.out.println("//form[contains(@class,'adult_"+i+" form_binder')]");
				 rowObj= driver.findElement(By.xpath("//form[contains(@class,'adult_"+i+" form_binder')]"));
			}else{
				
				
				System.out.println("//form[contains(@class,'child_"+i+" form_binder')]");
				 rowObj= driver.findElement(By.xpath("//form[contains(@class,'child_"+i+" form_binder')]"));
				
				
			}
				 
				 
				 Select selection_bag =new Select(rowObj.findElement(By.name("traveller[bin_bags]")));
				 TestUtil.selectBypartText(selection_bag, selectVals[0]);
				 
				 if (!noBaginfoFlag){
				try{
				 Thread.sleep(5000);	
				 driver.findElement(By.id("ok_zero_bags")).click();
				 noBaginfoFlag = true;
				}catch(Exception e){
				}
				
				 }
				 Select selection_chk =new Select(rowObj.findElement(By.name("traveller[checked_bags]")));
				 TestUtil.selectBypartText(selection_chk, selectVals[1] + " Bags");

				 
				 Select selection_prio =new Select(rowObj.findElement(By.name("traveller[priority_boarding_selected]")));
				 TestUtil.selectBypartText(selection_prio, selectVals[2]);
				 
				 
			 
			// for (int j=0;j<)
		 }
		//ok_zero_bags
		
		 driver.findElement(By.className("continue")).click();
		 try{
			 driver.findElement(By.className("continue")).sendKeys(Keys.ENTER);
		 }catch(Exception e){
			 System.out.println("test");
		 }
		 Thread.sleep(3000);
			
		}

}
