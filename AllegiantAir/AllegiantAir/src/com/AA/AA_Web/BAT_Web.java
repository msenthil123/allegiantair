package com.AA.AA_Web;

import java.io.IOException;
import java.sql.Date;

import junit.framework.Assert;

import org.apache.xmlbeans.impl.regex.REUtil;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


import com.AA.AA_Web.Reusable_AA_Web.BrowserKill;
import com.AA.AA_Web.Reusable_AA_Web.res_FlightSelection;
import com.AA.AA_Web.Reusable_AA_Web.res_Hotel_Offer;
import com.AA.AA_Web.Reusable_AA_Web.res_PaymentPage;
import com.AA.AA_Web.Reusable_AA_Web.res_SearchFlight;
import com.AA.AA_Web.Reusable_AA_Web.res_SeatSelection;
import com.AA.AA_Web.Reusable_AA_Web.res_SelectBags;
import com.AA.AA_Web.Reusable_AA_Web.res_Traveller;
import com.AA.AA_Web.Reusable_AA_Web.res_initialVerification;
import com.AA.AA_Web.Reusable_AA_Web.res_invokeApp;
import com.AA.AA_Web.Reusable_AA_Web.res_login;
import com.AA.util.TestUtil;

public class BAT_Web  extends TestBaseWeb {
	
	static int count=-1;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	static String confirmationNumber ="";
	
	
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(AAwebXls,"BAT")){
			APP_LOGS.debug("Skipping Test Case BAT as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case BAT as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		//runmodes=TestUtil.getDataSetRunmodes(suiteAxls, this.getClass().getSimpleName());
	}
	
	
	@Test (dataProvider="getTestData")
	public void BAT_Web_bs(
			 String runMode, String descriptTion, String reSult, String confNumber,
			 String userName, String passWord, String displayName,
			  String depCity, String destCity,
			 String tripType,  String adultsCount,String childCount,
			 String detpDate, String retDate, String child_month,
			 String child_date, String child_year, String adultsbagInfo,String childBagInfo,
			 String depSeats, String retSeats, String adultsFname,
			 String adultsLname, String adultsDOB, String adultsGender,
			 String childFname, String childLname, String childGender
			 
			) throws InterruptedException, IOException{ 
		confirmationNumber ="";
		count ++;
		
		
		if(!runMode.equalsIgnoreCase("Y")){
			skip=true;
			throw new SkipException("Runmode for test set data set to no "+count);
		}
		
		
		APP_LOGS.debug(" Executing BAT");
		APP_LOGS.debug(runMode +" -- "+descriptTion +" -- "+reSult +" -- "+userName + " -- " + detpDate 
				      +" -- " + retDate + " -- " + adultsCount + " -- " + childCount);
		//APP_LOGS.debug(TestUtil.getDatefromToday( Integer.parseInt(retDate)));
		
		//System.out.println("BAT_Web_bs");
		BrowserKill.killAll();
		//Open the application
	    res_invokeApp.res_Invokeapplication("FireFox");
	    
		
		//Verify initial conidtions
		res_initialVerification.res_InitialVer();
		
		//Login
		if (!res_login.res_Login(userName, passWord, displayName))
		{
		fail = true;
                      System.out.println("Login is failed");
		}
		else
                {
                           System.out.println("Successfully logged in");
                }
		
			
		if (!res_SearchFlight.res_searchflight(depCity, destCity, tripType, detpDate, retDate, adultsCount, childCount, child_month, child_date, child_year))
		{
		    fail = true;
		}
		
		res_FlightSelection.verifyFlightSelectionPageinfo(tripType);
		
		Thread.sleep(10000);
		
		res_Hotel_Offer.res_HotelOffer();
		
		Thread.sleep(2000);
		
		res_Traveller.res_TravellerForm_Filling( adultsCount, adultsFname, adultsLname, adultsDOB, adultsGender, childCount, childFname, childLname, childGender);
		
		//Selecting Seats
		res_SeatSelection.selectSeatMain(depSeats, retSeats, tripType);
		
		//Selecting Bags
		res_SelectBags.res_BagSelection(adultsbagInfo, "adult");
		System.out.println(childCount);
		if (childBagInfo != null && childBagInfo.trim() !="")
		{
			res_SelectBags.res_BagSelection(childBagInfo, "child");

		}
		
		Thread.sleep(3000);
		confirmationNumber = res_PaymentPage.res_PaymentInfoFilling(userName);
		if (confirmationNumber == "")
		{
			fail = true;
		}
		res_invokeApp.driver.quit();
		
		}
	
	
	
	     
		//
		
		//String exp_value = "Senthil";
		//String act_value = "Senthil";
		
		//dispName
		
			
	
	@AfterMethod
	public void reportDataSetResult(){
		if(skip)
			TestUtil.reportDataSetResult(AAwebXls, "BAT", count+2, "SKIP", confirmationNumber);
		else if(fail){
			isTestPass=false;
			TestUtil.reportDataSetResult(AAwebXls, "BAT", count+2, "FAIL", confirmationNumber);
		}
		else
			TestUtil.reportDataSetResult(AAwebXls, "BAT", count+2, "PASS", confirmationNumber);
		
		skip=false;
		fail=false;
		confirmationNumber="";

	}
	
	@AfterTest
	public void reportTestResult(){
		if(isTestPass)
			TestUtil.reportDataSetResult(AAwebXls, "TestNames", TestUtil.getRowNum(AAwebXls,"Test Names","BAT"), "PASS","");
		else
			TestUtil.reportDataSetResult(AAwebXls, "TestNames", TestUtil.getRowNum(AAwebXls,"Test Names","BAT"), "FAIL","");
	
	}
	
	
	@DataProvider
	public Object[][] getTestData(){
		return TestUtil.getData(AAwebXls, "BAT");
	}

}
