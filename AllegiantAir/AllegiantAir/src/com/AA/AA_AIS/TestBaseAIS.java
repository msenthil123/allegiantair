package com.AA.AA_AIS;

import org.testng.SkipException;
import org.testng.annotations.BeforeSuite;

import com.AA.base.TestBase;
import com.AA.util.TestUtil;

public class TestBaseAIS extends TestBase {

	// check if the suite ex has to be skiped
				@BeforeSuite
				public void checkSuiteSkip() throws Exception{
					initialize();
					APP_LOGS.debug("Checking Runmode of AllegiantAirAIS");
					if(!TestUtil.isSuiteRunnable(suiteXls, "AllegiantAirAIS")){
						APP_LOGS.debug("Skipped AllegiantAirAIS as the runmode was set to NO");
						throw new SkipException("RUnmode of AllegiantAirAIS set to no. So Skipping all tests in AllegiantAirAIS");
					}
					
				}
	
}
